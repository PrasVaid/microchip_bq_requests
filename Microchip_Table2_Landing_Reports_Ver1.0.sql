
#Landing Page Report

select
     date,
     Landing_Page,
     PageTitle,
     TrafficSource,
     Medium,
     Campaign,
     AdContent,
     COUNT(bounces) AS Bounces, 
     COUNT(hits) AS Entrances,
     sum(Exits) as Exits,
     COUNT(*) as pageviews,
     COUNT(DISTINCT session_id) AS unique_pageviews,
     SUM(timeOnSite) AS Total_time,
     SUM(timeOnSite)/COUNT(session_id) AS AvgTimeOnSite
     FROM
(SELECT
     date,
     hits.page.pagePath AS Landing_Page,
     hits.page.pageTitle AS PageTitle,
     trafficSource.source as TrafficSource,
     trafficSource.medium as Medium,
     trafficSource.campaign as Campaign,
     trafficSource.adContent as AdContent,
     totals.bounces, 
     totals.hits,
     totals.timeOnSite,
     CASE WHEN hits.isExit IS NOT NULL THEN 1 ELSE 0 END AS Exits,
     CONCAT(fullVisitorId, CAST(visitStartTime AS STRING)) AS session_id
FROM 
     `rising-timing-237717.5155215.ga_sessions_20200103` AS GA,
     UNNEST(GA.hits) AS hits
WHERE 
     hits.type="PAGE" AND hits.hitNumber=1

)
     GROUP BY 
     date,Landing_Page,PageTitle,TrafficSource,Medium,Campaign,AdContent