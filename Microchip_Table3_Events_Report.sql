

SELECT
  CONCAT(substr(date,5,2),substr(date,1,4),substr(date,7,2)) as Date,
  hits.page.pagePath AS Page,
  trafficSource.source as TrafficSource,
  trafficSource.medium as Medium,
  geoNetwork.country as Country,
  -- Event Category (dimension)
  hits.eventInfo.eventCategory AS Event_Category,
  -- Event Action (dimension)
  hits.eventInfo.eventAction AS Event_Action,
  -- Event Label (dimension)
  hits.eventInfo.eventLabel AS Event_Label,
  -- Total Events (metric)
  COUNT(*) AS Total_Events,
  -- Unique Events (metric),
  COUNT(DISTINCT CONCAT(CAST(fullVisitorId AS STRING), CAST(visitStartTime AS STRING))) AS Unique_Events,
  -- Event Value (metric)
 # SUM(hits.eventInfo.eventValue) AS Event_Value,
  -- Avg. Value (metric)
#  SUM(hits.eventInfo.eventValue) / COUNT(*) AS Avg_Value,
  -- Sessions With Events (metric)
#  COUNT(DISTINCT
#    CASE
#      WHEN hits.type = 'EVENT' THEN CONCAT(CAST(fullVisitorId AS STRING), CAST(visitStartTime AS STRING))
#    ELSE
#    NULL
#  END
#    ) AS Sessions_With_Events,
  -- Events / Session With Event (metric)
#  COUNT(*) / COUNT(DISTINCT
#    CASE
#      WHEN hits.type = 'EVENT' THEN CONCAT(CAST(fullVisitorId AS STRING), CAST(visitStartTime AS STRING))
#    ELSE
#   NULL
#  END
#    ) AS Events_Session_With_Event
FROM
  `rising-timing-237717.5155215.ga_sessions_*`,
  UNNEST(hits) AS hits
WHERE
  _table_suffix BETWEEN '20200119'
  AND FORMAT_DATE('%Y%m%d',DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY))
  AND totals.visits = 1
  AND hits.type = 'EVENT'
GROUP BY
  1,2,3,4,5,6,7,8
HAVING
  Event_Category IS NOT NULL
ORDER BY
  1,2,3,4,5,6,7,8
