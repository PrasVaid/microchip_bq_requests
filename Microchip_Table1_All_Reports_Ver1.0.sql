

#### intial try #### 
select 
date,
hits.page.pagePath,
hits.page.pageTitle,
GA.channelGrouping,
GA.trafficSource.source as Source,
GA.trafficSource.medium as Medium,
GA.trafficSource.campaign as Campaign,
GA.trafficSource.adContent as AdContent,
GA.geoNetwork.country 
from `rising-timing-237717.5155215.ga_sessions_20200103` as GA,
Unnest(GA.hits) as hits



#### Update Query_1 ####

SELECT
  date,
  pagePath,
  pageTitle,
  TrafficSource,
  Medium,
  Campaign,
  AdContent,
  bounces,
  sessions,
  pageviews,
  unique_pageviews,
  CASE WHEN sessions = 0 THEN 0 ELSE bounces / sessions END AS bounce_rate,
  CASE WHEN sessions = 0 THEN 0 ELSE pageviews / sessions END AS pageviews_per_session
FROM (
  SELECT
    date,
    pagePath,
    pageTitle,
    TrafficSource,
    Medium,
     Campaign,
     AdContent,
    COUNT(*) as pageviews,
    COUNT(DISTINCT session_id) AS unique_pageviews,
    SUM(bounces) AS bounces,
    SUM(sessions) AS sessions
  FROM (
    SELECT
      date,
      TrafficSource,
      Medium,
      Campaign,
      AdContent,
      fullVisitorId,
      visitStartTime,
      pagePath,
      pageTitle,
      session_id,
      CASE WHEN hitNumber = first_interaction THEN bounces ELSE 0 END AS bounces,
      CASE WHEN hitNumber = first_hit THEN visits ELSE 0 END AS sessions
    FROM (
      SELECT
        date,
        trafficSource.source as TrafficSource,
        trafficSource.medium as Medium,
        trafficSource.campaign as Campaign,
        trafficSource.adContent as AdContent,
        geoNetwork.country 
        fullVisitorId,
        visitStartTime,
        hits.page.pagePath AS pagePath,
        hits.page.pageTitle AS pageTitle,
        totals.bounces,
        totals.visits,
        hits.hitNumber,
        CONCAT(fullVisitorId, CAST(visitStartTime AS STRING)) AS session_id,
        MIN(IF(hits.isInteraction IS NOT NULL, hits.hitNumber, 0)) OVER (PARTITION BY fullVisitorId, visitStartTime) AS first_interaction,
        MIN(hits.hitNumber) OVER (PARTITION BY fullVisitorId, visitStartTime) AS first_hit
      FROM `rising-timing-237717.5155215.ga_sessions_20200103`,
        UNNEST(hits) AS hits
      )
    )
    GROUP BY pagePath, date,TrafficSource,Medium,Campaign,AdContent,pageTitle
  )
ORDER BY date DESC;