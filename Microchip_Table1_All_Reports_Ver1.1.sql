  
## Bounces, Session and Page views

  SELECT
    date,
    pagePath,
    channelGrouping,
    pageTitle,
    TrafficSource,
    Medium,
     Campaign,
     AdContent,
    COUNT(*) as pageviews,
    COUNT(DISTINCT session_id) AS unique_pageviews,
    SUM(bounces) AS bounces,
    SUM(sessions) AS sessions
  FROM (
    SELECT
      date,
      TrafficSource,
      channelGrouping,
      Medium,
      Campaign,
      AdContent,
      fullVisitorId,
      visitStartTime,
      pagePath,
      pageTitle,
      session_id,
      CASE WHEN hitNumber = first_interaction THEN bounces ELSE 0 END AS bounces,
      CASE WHEN hitNumber = first_hit THEN visits ELSE 0 END AS sessions
    FROM (
      SELECT
        date,
        trafficSource.source as TrafficSource,
        channelGrouping,
        trafficSource.medium as Medium,
        trafficSource.campaign as Campaign,
        trafficSource.adContent as AdContent,
        geoNetwork.country 
        fullVisitorId,
        visitStartTime,
        hits.page.pagePath AS pagePath,
        hits.page.pageTitle AS pageTitle,
        totals.bounces,
        totals.visits,
        hits.hitNumber,
        CONCAT(fullVisitorId, CAST(visitStartTime AS STRING)) AS session_id,
        MIN(IF(hits.isInteraction IS NOT NULL, hits.hitNumber, 0)) OVER (PARTITION BY fullVisitorId, visitStartTime) AS first_interaction,
        MIN(hits.hitNumber) OVER (PARTITION BY fullVisitorId, visitStartTime) AS first_hit
      FROM `rising-timing-237717.5155215.ga_sessions_20200103`,
        UNNEST(hits) AS hits
      )
    )
    GROUP BY date,pagePath ,pageTitle ,channelGrouping ,Medium,TrafficSource ,Campaign, AdContent
  
ORDER BY date,pagePath,pageTitle,channelGrouping,Medium,TrafficSource,Campaign, AdContent;


## ADDING Entrances and Exits 
## Check with Eric if adding groupby in first loop is going to work?
## Trying using hits.istype in first loop as given in the time calcualtion query
## Done was able to combine Exit and Entrances without groupby in first select query loop

SELECT
    date,
    pagePath,
    channelGrouping,
    pageTitle,
    TrafficSource,
    Medium,
     Campaign,
     AdContent,
    COUNT(*) as pageviews,
    COUNT(DISTINCT session_id) AS unique_pageviews,
    SUM(bounces) AS bounces,
    SUM(Entrances) AS Entrances,
      SUM(Exits) AS Exits
  FROM (
    SELECT
      date,
      TrafficSource,
      channelGrouping,
      Medium,
      Campaign,
      AdContent,
      fullVisitorId,
      visitStartTime,
      pagePath,
      pageTitle,
      session_id,
      CASE WHEN isEntrance IS NOT NULL THEN 1 ELSE 0 END AS Entrances,
      CASE WHEN isExit IS NOT NULL THEN 1 ELSE 0 END AS Exits,
      CASE WHEN hitNumber = first_interaction THEN bounces ELSE 0 END AS bounces,
      CASE WHEN hitNumber = first_hit THEN visits ELSE 0 END AS sessions,
    FROM (
      SELECT
        date,
        trafficSource.source as TrafficSource,
        channelGrouping,
        trafficSource.medium as Medium,
        trafficSource.campaign as Campaign,
        trafficSource.adContent as AdContent,
        geoNetwork.country 
        fullVisitorId,
        visitStartTime,
        hits.page.pagePath AS pagePath,
        hits.page.pageTitle AS pageTitle,
        totals.bounces,
        totals.visits,
        hits.hitNumber,
        hits.type,
        hits.isEntrance ,
        hits.isExit,
        CONCAT(fullVisitorId, CAST(visitStartTime AS STRING)) AS session_id,
        MIN(IF(hits.isInteraction IS NOT NULL, hits.hitNumber, 0)) OVER (PARTITION BY fullVisitorId, visitStartTime) AS first_interaction,
        MIN(hits.hitNumber) OVER (PARTITION BY fullVisitorId, visitStartTime) AS first_hit
      FROM `rising-timing-237717.5155215.ga_sessions_20200103`,
        UNNEST(hits) AS hits
      )
        
    )
    GROUP BY date,pagePath ,pageTitle ,channelGrouping ,Medium,TrafficSource ,Campaign, AdContent
   
ORDER BY date,pagePath,pageTitle,channelGrouping,Medium,TrafficSource,Campaign, AdContent;




### Query for Total time on page and Avg Time on page
SELECT
  date,
  TrafficSource,
  channelGrouping,
  Medium,
  Campaign,
  AdContent,
  pagePath,
  pageTitle,
  pageviews,
  exits,
  total_time_on_page,
  CASE
    WHEN pageviews = exits THEN 0
    ELSE total_time_on_page / (pageviews - exits)
  END AS avg_time_on_page
FROM (
  SELECT
  date,
      TrafficSource,
      channelGrouping,
      Medium,
      Campaign,
      AdContent,
    pagePath,
    pageTitle,
    COUNT(*) AS pageviews,
    SUM(IF(isExit IS NOT NULL,
        1,
        0)) AS exits,
    SUM(time_on_page) AS total_time_on_page
  FROM (
    SELECT
    date,
      TrafficSource,
      channelGrouping,
      Medium,
      Campaign,
      AdContent,
      fullVisitorId,
      visitStartTime,
      pagePath,
      pageTitle,
      hit_time,
      type,
      isExit,
      CASE
        WHEN isExit IS NOT NULL THEN last_interaction - hit_time
        ELSE next_pageview - hit_time
      END AS time_on_page
    FROM (
      SELECT
      date,
      TrafficSource,
      channelGrouping,
      Medium,
      Campaign,
      AdContent,
        fullVisitorId,
        visitStartTime,
        pagePath,
        pageTitle,
        hit_time,
        type,
        isExit,
        last_interaction,
        LEAD(hit_time) OVER (PARTITION BY fullVisitorId, visitStartTime ORDER BY hit_time) AS next_pageview
      FROM (
        SELECT
        date,
      TrafficSource,
      channelGrouping,
      Medium,
      Campaign,
      AdContent,
          fullVisitorId,
          visitStartTime,
          pagePath,
          pageTitle,
          hit_time,
          type,
          isExit,
          last_interaction
        FROM (
          SELECT
            date,
        trafficSource.source as TrafficSource,
        channelGrouping,
        trafficSource.medium as Medium,
        trafficSource.campaign as Campaign,
        trafficSource.adContent as AdContent,
            fullVisitorId,
            visitStartTime,
            hits.page.pagePath,
            hits.page.pageTitle AS pageTitle,
            hits.type,
            hits.isExit,
            hits.time / 1000 AS hit_time,
            MAX(IF(hits.isInteraction IS NOT NULL,
                hits.time / 1000,
                0)) OVER (PARTITION BY fullVisitorId, visitStartTime) AS last_interaction
          FROM
            `rising-timing-237717.5155215.ga_sessions_20200103` AS GA,
            UNNEST(GA.hits) AS hits)
        WHERE
          type = 'PAGE')))
  GROUP BY
    pagePath,pageTitle, date,TrafficSource,Medium,Campaign,AdContent,channelGrouping)
    
  ORDER BY date,pagePath,pageTitle,channelGrouping,Medium,TrafficSource,Campaign, AdContent; 
